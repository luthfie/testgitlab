<?php
/* testgitlab class
 * ~ test for gitlab repository in AI
 * ~ extension for AI
 * authored by 9r3i
 * https://github.com/9r3i
 * started at july 27th 2019
 */
class testgitlab{
  const version='1.0.0';
  const info='Testing tool for gitlab repository.';
  private $host='https://gitlab.com/%s/%s/raw/%s/%s';
  /* check --> $file => <user>/<repo>/<branch>/<file> */
  function check($file=null){
    /* check file argument */
    if(!is_string($file)){return false;}
    /* explode file argument */
    $ex=explode('/',$file);
    /* check explode result */
    if(count($ex)!=4){return false;}
    /* prepare url */
    $url=sprintf($this->host,$ex[0],$ex[1],$ex[2],$ex[3]);
    /* get secure */
    $get=aiSecure::stream($url);
    /* check result */
    if(!$get){return false;}
    /* return get result */
    return $get;
  }
  /* help */
  function help(){
    $info=$this::info;
    $version=$this::version;
    return <<<EOD
{$info}
Version {$version}

  $ AI TESTGITLAB <option> [arguments]

Options:
  CHECK   Check package file.

Example:
  $ AI TESTGITLAB CHECK <path/to/file>
EOD;
  }
}
