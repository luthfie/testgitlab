[![Author](https://img.shields.io/badge/author-Abu_Ayyub-lightgrey.svg)](https://gitlab.com/luthfie)
[![License](https://img.shields.io/badge/license-Apache2.0-lightblue.svg)](https://gitlab.com/luthfie/test/blob/master/license.txt)


# Testing Script
- This is only a testing script for AI.


# Installation
- For AI users, simply use command:

```
$ ai pkg addrepository luthfie/testgitlab
$ ai install ext.testgitlab
```


# Testing Check
- For AI users, sample commands:

```
$ ai testgitlab check <path/to/file>
```

- Use ```$ ai testgitlab help``` for more detail.


